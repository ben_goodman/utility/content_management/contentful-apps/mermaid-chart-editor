import { type ConfigAppSDK } from '@contentful/app-sdk'
import { type CreateContentTypeProps, type Control } from 'contentful-management'


export const createContentType = async (
    contentTypeId: string,
    initialContentTypeProps: CreateContentTypeProps,
    sdk: ConfigAppSDK,
): Promise<void> => {
    const cma = sdk.cma
    console.log('Creating custom content type: ', contentTypeId)
    const contentTypeProps = await cma.contentType.createWithId(
        { contentTypeId },
        initialContentTypeProps
    )

    const newContentType =
        await cma.contentType.publish({ contentTypeId }, contentTypeProps )
    console.log('Created custom content type: ', newContentType)
}

export const updateFieldControls = async (
    contentTypeId: string,
    fieldControls: Control[],
    sdk: ConfigAppSDK,
): Promise<void> => {
    const cma = sdk.cma

    console.log('Adding field controls to custom content type: ', fieldControls)
    const editorInterface = await cma.editorInterface.get({contentTypeId})
    console.log('Current editor interface: ', editorInterface)

    editorInterface.controls = fieldControls
    const updatedInterface =
        await cma.editorInterface.update({contentTypeId}, editorInterface)
    console.log('Updated editor interface: ', updatedInterface)
}


export const hasContentType = async (
    contentTypeId: string,
    sdk: ConfigAppSDK
): Promise<boolean> => {
    const cma = sdk.cma

    console.log('Checking for content type: ', contentTypeId)
    return !! await cma.contentType.get({contentTypeId}).catch(e => {
        console.log(e)
        if (e.code === 'NotFound') {
            return false
        }
        throw e
    })
}

export const hasFieldControlSet = async (
    contentTypeId: string,
    fieldControls: Control[],
    sdk: ConfigAppSDK
): Promise<boolean> => {
    const cma = sdk.cma

    console.log('Checking for field controls: ', fieldControls)
    const editorInterface = await cma.editorInterface.get({contentTypeId})
    console.log('Current editor interface: ', editorInterface)

    const { controls } = editorInterface

    const q = controls?.find(control => {
        const matchingControl = fieldControls.find(fieldControl => {
            console.log('Checking field control: ', fieldControl)
            return control.widgetNamespace === fieldControl.widgetNamespace
                && control.widgetId === fieldControl.widgetId
        })
        console.log('Matching control: ', matchingControl)
        return matchingControl
    })

    return !!q
}