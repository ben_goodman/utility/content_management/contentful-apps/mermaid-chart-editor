import { type CreateContentTypeProps } from 'contentful-management'
import { AppInstallationParameters } from '../locations/ConfigScreen'

const capitalize = (str: string) => {
    const words = (str.match(/([A-Z]?[^A-Z]*)/g) || []).slice(0,-1)
    return words.map(w => w.charAt(0).toUpperCase() + w.slice(1)).join(' ')
}

export const mermaidChartPropsMin = (
    appInstallationParameters: AppInstallationParameters,
) => {
    const {
        chartFieldId,
        mermaidVersionFieldId,
        mermaidVersion,
        locale
    } = appInstallationParameters

    return [{
        id: chartFieldId,
        name: capitalize(chartFieldId),
        type: 'Text',
        localized: false,
        required: true,
        validations: [],
        disabled: false,
        omitted: false
    },
    {
        id: mermaidVersionFieldId,
        name: capitalize(mermaidVersionFieldId),
        type: 'Symbol',
        localized: false,
        required: true,
        validations: [],
        disabled: false,
        omitted: false,
        defaultValue: {
            [locale]: mermaidVersion
        }
    }]
}

export const mermaidChartContentTypeProps = (
    appInstallationParameters: AppInstallationParameters,
): CreateContentTypeProps => {
    return {
        name: appInstallationParameters.contentTypeName,
        description: 'A mermaid chart with optional fields for a caption and alt-text.',
        displayField: 'title',
        fields: [
            {
                id: 'title',
                name: 'Title',
                type: 'Symbol',
                localized: false,
                required: true,
                validations: [
                    {
                        'unique': true
                    }
                ],
                disabled: false,
                omitted: false
            },
            ...mermaidChartPropsMin(appInstallationParameters),
            {
                id: 'description',
                name: 'Description',
                type: 'Symbol',
                localized: false,
                required: true,
                validations: [],
                disabled: false,
                omitted: false
            },
            {
                id: 'caption',
                name: 'Caption',
                type: 'Text',
                localized: false,
                required: false,
            },
        ]
    }
}