import React, { useCallback, useState, useEffect } from 'react'
import { ConfigAppSDK } from '@contentful/app-sdk'
import { Heading, Form, Paragraph, Flex, Accordion } from '@contentful/f36-components'
import { css } from 'emotion'
import { useSDK } from '@contentful/react-apps-toolkit'
import { type Control } from 'contentful-management'
import { createContentType, hasContentType, hasFieldControlSet, updateFieldControls } from '../util/createContentType'
import { mermaidChartContentTypeProps } from '../util/mermaidChartProps'
import ConfigItems from '../components/ConfigItems'
import { AppInstallationEdit } from '../components/AppInstallationEdit'

const DEFAULT_CONTENT_TYPE_ID = 'mermaid-chart-v1'
const DEFAULT_CHART_FIELD_ID = 'chart'
const DEFAULT_VERSION_FIELD_ID = 'mermaidVersion'
const DEFAULT_MERMAID_VERSION = '10.5.0'
const DEFAULT_CONTENT_TYPE_NAME = 'Mermaid Chart'

export interface AppInstallationParameters {
    isConfigDisabled: boolean
    isContentTypeSet: boolean
    isFieldAppearanceSet: boolean
    userCheckAutoInstall: boolean
    chartFieldId: string
    mermaidVersionFieldId: string
    contentTypeId: string
    contentTypeName: string
    mermaidVersion: string
    locale: string
}

const DEFAULT_APP_PARAMS: AppInstallationParameters = {
    isConfigDisabled: false,
    isContentTypeSet: false,
    isFieldAppearanceSet: false,
    userCheckAutoInstall: true,
    chartFieldId: DEFAULT_CHART_FIELD_ID,
    mermaidVersionFieldId: DEFAULT_VERSION_FIELD_ID,
    contentTypeId: DEFAULT_CONTENT_TYPE_ID,
    contentTypeName: DEFAULT_CONTENT_TYPE_NAME,
    mermaidVersion: DEFAULT_MERMAID_VERSION,
    locale: 'en-US'
}

export interface ConfigAccordionState {
    auto: boolean
    manual: boolean
}

const DEFAULT_CONFIG_ACCORDION_STATE: ConfigAccordionState = {
    auto: true,
    manual: false
}

const ConfigScreen = () => {
    const [parameters, setParameters] = useState<AppInstallationParameters>(
        DEFAULT_APP_PARAMS
    )

    const [accordionState, setAccordionState]
        = useState(DEFAULT_CONFIG_ACCORDION_STATE)

    const sdk = useSDK<ConfigAppSDK>()

    const fieldControls: Control[] = [
        {
            fieldId: parameters.chartFieldId,
            widgetId: sdk.ids.app,
            widgetNamespace: 'app',
        }
    ]

    // check if the 'MermaidChart' content type exists and has been configured.
    useEffect(() => {
        (async () => {
            const isContentTypeSet =
                await hasContentType(parameters.contentTypeId, sdk)

            const isFieldAppearanceSet
                = isContentTypeSet && await hasFieldControlSet(
                    parameters.contentTypeId,
                    fieldControls,
                    sdk
                )

            const isConfigDisabled = await sdk.app.isInstalled()

            const locale = sdk.locales.default

            setParameters(s => ({
                ...s,
                isContentTypeSet,
                isFieldAppearanceSet,
                isConfigDisabled,
                locale
            }))
        })()
    }, [])

    // handle checkbox for auto-installation of the 'MermaidChart' content type.
    const handleCheckboxChange = (
        e?: React.ChangeEvent<HTMLInputElement>
    ): void => {
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        const { checked } = e!.target
        if (!checked) {
            setAccordionState({auto: false, manual: true})
        }
        setParameters(s => ({...s, userCheckAutoInstall: checked}))
    }

    const onConfigure = useCallback(async () => {
    // This method will be called when a user clicks on "Install"
    // or "Save" in the configuration screen.
        console.log('onConfigure')
        const {
            userCheckAutoInstall,
            isContentTypeSet,
            contentTypeId,
        } = parameters

        //  disable the configuration form
        setParameters(s => ({...s, isConfigDisabled: true}))

        // Create the content type if it doesn't exist and the user has requested it.
        if (!isContentTypeSet && userCheckAutoInstall) {
            await createContentType(
                contentTypeId,
                mermaidChartContentTypeProps(parameters),
                sdk,
            )
            await hasContentType(contentTypeId, sdk)
                .then((isContentTypeSet) => {
                    setParameters(s => ({...s, isContentTypeSet}))
                })
        }

        // Get current the state of EditorInterface and other entities
        // related to this app installation
        const currentState = await sdk.app.getCurrentState()

        return {
            // Parameters to be persisted as the app configuration.
            parameters,
            // In case you don't want to submit any update to app
            // locations, you can just pass the currentState as is
            targetState: currentState,
        }
    }, [parameters, sdk])

    useEffect(() => {
    // `onConfigure` allows to configure a callback to be
    // invoked when a user attempts to install the app or update
    // its configuration.
        sdk.app.onConfigure(() => onConfigure())
    }, [sdk, onConfigure])

    useEffect(() => {
        (async () => {
            // Get current parameters of the app.
            // If the app is not installed yet, `parameters` will be `null`.
            const currentParameters: AppInstallationParameters | null =
                await sdk.app.getParameters()

            console.log('currentParameters', currentParameters)

            if (currentParameters) {
                setParameters(currentParameters)
            }

            // Once preparation has finished, call `setReady` to hide
            // the loading screen and present the app to a user.
            await sdk.app.setReady()
        })()
    }, [sdk])

    const setFieldAppearance = () => {
        (async () => {
            if (
                parameters.isContentTypeSet
                && !parameters.isFieldAppearanceSet
            ) {
                const isInstalled = await sdk.app.isInstalled()
                if (isInstalled) {
                    console.log('App is installed.  Setting field appearance.')
                    updateFieldControls(
                        parameters.contentTypeId,
                        fieldControls,
                        sdk
                    ).then(() => {
                        setParameters(s => ({...s, isFieldAppearanceSet: true}))
                    })
                }
            }
        })()
    }

    return (
        <Flex>

            <Flex
                flexDirection="column"
                className={css({ margin: '80px', maxWidth: '800px' })}
            >
                <Form>
                    <Heading>Mermaid Chart Editor Configuration</Heading>

                    <Paragraph>
                        This application provides a custom text field editor
                        for rendering a Mermaid chart within a preview pane.
                        The editor can be added to an existing field or
                        automatically set within a new content type.
                    </Paragraph>

                    <Accordion>
                        <ConfigItems.Auto
                            appInstallationParameters={parameters}
                            accordionState={accordionState}
                            setAccordionState={setAccordionState}
                            onChange={handleCheckboxChange}
                            onFieldSetClick={setFieldAppearance}
                        />
                        <ConfigItems.Manual
                            appInstallationParameters={parameters}
                            accordionState={accordionState}
                            setAccordionState={setAccordionState}
                        />
                    </Accordion>

                </Form>
            </Flex>

            <AppInstallationEdit
                appInstallationParameters={parameters}
                setParameters={setParameters}
            />


        </Flex>
    )
}

export default ConfigScreen
