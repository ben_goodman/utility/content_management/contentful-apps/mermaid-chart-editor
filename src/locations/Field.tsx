import React, { useEffect, useState } from 'react'
import { Box, FormControl, Tabs } from '@contentful/f36-components'
import { type EntryAPI, type FieldAppSDK } from '@contentful/app-sdk'
import { MultipleLineEditor } from '@contentful/field-editor-multiple-line'
import { useSDK } from '@contentful/react-apps-toolkit'
import styled from 'styled-components'
import { MermaidChart } from '../components/MermaidChart'
import { type AppInstallationParameters } from './ConfigScreen'


const StyledMultilineEditor = styled.div`
    textarea {
        min-height: 300px;
        margin-top: 3px;
    }
`

const Field = () => {
    const sdk = useSDK<FieldAppSDK>()
    const entry = sdk.entry as EntryAPI
    const chartField = entry.fields[
        (sdk.parameters.installation as AppInstallationParameters)
            .chartFieldId
    ]
    const versionField = entry.fields[
        (sdk.parameters.installation as AppInstallationParameters)
            .mermaidVersionFieldId
    ]

    const {mermaidVersion} = sdk.parameters.installation

    const [
        chartInputValue,
        setChartInputValue
    ] = useState(chartField.getValue())

    const [
        mermaidVersionInputValue,
        setMermaidVersionInputValue
    ] = useState(versionField.getValue())

    // allows the iframe'd app to resize itself inside the Contentful UI
    useEffect(() => {
        sdk.window.startAutoResizer()
        return sdk.window.stopAutoResizer
    }, [sdk])

    // passes user input to the read-only chart preview
    useEffect( () => {
        const detachChangeHandler = chartField.onValueChanged((value) => {
            if (value !== chartInputValue) {
                setChartInputValue(value)
            }
        })
        return detachChangeHandler
    }, [chartField, chartInputValue])

    useEffect( () => {
        const detachChangeHandler = versionField.onValueChanged((value) => {
            if (value !== mermaidVersionInputValue) {
                setMermaidVersionInputValue(value)
            }
        })
        return detachChangeHandler
    }, [versionField, mermaidVersionInputValue, setMermaidVersionInputValue])

    return (
        <Box style={{minHeight: '300px', marginRight: '5px'}}>
            <Tabs
                defaultTab="preview"
            >

                <Tabs.List variant="horizontal-divider">
                    <Tabs.Tab panelId="editor">Editor</Tabs.Tab>
                    <Tabs.Tab panelId="preview">Preview</Tabs.Tab>
                </Tabs.List>

                <Tabs.Panel id="editor">
                    <FormControl isRequired>
                        <StyledMultilineEditor>
                            <MultipleLineEditor
                                field={sdk.field}
                                locales={sdk.locales}
                            />
                        </StyledMultilineEditor>
                    </FormControl>
                </Tabs.Panel>

                <Tabs.Panel id="preview">
                    <MermaidChart
                        chart={chartInputValue}
                        version={mermaidVersion}
                    />
                </Tabs.Panel>

            </Tabs>
        </Box>
    )
}

export default Field
