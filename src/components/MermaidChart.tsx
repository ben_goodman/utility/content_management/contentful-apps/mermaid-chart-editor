import React, { type JSX, useEffect } from 'react'
import styled from 'styled-components'

declare global {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    interface Window { mermaid: any; }
}

window.mermaid = window.mermaid || {}

const DEFAULT_CONFIG = {
    startOnLoad: true,
    theme: 'wireframe',
    logLevel: 'fatal',
    securityLevel: 'loose',
    arrowMarkerAbsolute: false,
}

const Chart = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;

    pre.mermaid {
        width: 100%;
        height: 100%;
    }

    pre.mermaid > svg {
        display: block;
        margin: auto;
    }
`

const loadScript = (src: string) => {
    return new Promise<void>((resolve, reject) => {
        if (typeof window !== 'undefined') {
            const script = window.document.createElement('script')
            script.src = src
            script.addEventListener('load', function () {
                resolve()
            })
            script.addEventListener('error', function (e) {
                reject(e)
            })
            window.document.body.appendChild(script)
        }
    })
}

export interface MermaidChartProps {
    chart: string
    version?: string
}

export const MermaidChart = ({
    chart,
    version,
}: MermaidChartProps ): JSX.Element => {
    useEffect(() => {
        const renderMermaid = async () => {
            if (!window.mermaid.mermaidAPI) {
                await loadScript(`https://cdnjs.cloudflare.com/ajax/libs/mermaid/${version}/mermaid.min.js`)
            }
            window.mermaid.mermaidAPI.initialize(DEFAULT_CONFIG)
            window.mermaid.contentLoaded()
        }
        void renderMermaid()
    }, [])

    return (

        <Chart>
            <pre className="mermaid">{chart}</pre>
        </Chart>
    )
}