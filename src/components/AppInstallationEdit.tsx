import React, { Dispatch, SetStateAction } from 'react'
import { Flex, Form, FormControl, TextInput } from '@contentful/f36-components'
import { AppInstallationParameters } from '../locations/ConfigScreen'

export interface AppInstallationEditProps {
    appInstallationParameters: AppInstallationParameters
    setParameters: Dispatch<SetStateAction<AppInstallationParameters>>
}

interface SetParamInputProps {
    appInstallationParameters: AppInstallationParameters
    onChange: (e: React.ChangeEvent<HTMLInputElement>) => void
}

const SetMermaidVersion = ({
    appInstallationParameters,
    onChange
}: SetParamInputProps) => {
    const {
        mermaidVersion,
        isConfigDisabled
    } = appInstallationParameters

    return (
        <FormControl
            isRequired
            isInvalid={!mermaidVersion}
            isDisabled={isConfigDisabled}
        >
            <FormControl.Label>Mermaid Version</FormControl.Label>
            <TextInput
                value={mermaidVersion}
                name="mermaidVersion"
                onChange={onChange}
            />
            <FormControl.HelpText>
            Provide the version of Mermaid you want to use.
            </FormControl.HelpText>
            {!mermaidVersion && (
                <FormControl.ValidationMessage>
    Please, provide a Mermaid release version.
                </FormControl.ValidationMessage>
            )}
        </FormControl>
    )

}

const SetMermaidVersionFieldId = ({
    appInstallationParameters,
    onChange
}: SetParamInputProps) => {
    const {
        mermaidVersionFieldId,
        isConfigDisabled
    } = appInstallationParameters

    return (
        <FormControl
            isRequired
            isInvalid={!mermaidVersionFieldId}
            isDisabled={isConfigDisabled}
        >
            <FormControl.Label>Mermaid Version Field ID</FormControl.Label>
            <TextInput
                value={mermaidVersionFieldId}
                name="mermaidVersionFieldId"
                onChange={onChange}
            />
            <FormControl.HelpText>
            Provide the ID of the field you
            want to use for Mermaid version input.
            </FormControl.HelpText>
            {!mermaidVersionFieldId && (
                <FormControl.ValidationMessage>
    Please, provide a field ID.
                </FormControl.ValidationMessage>
            )}
        </FormControl>
    )
}

const SetChartFieldId = ({
    appInstallationParameters,
    onChange
}: SetParamInputProps) => {
    const {
        chartFieldId,
        isConfigDisabled
    } = appInstallationParameters

    return (
        <FormControl
            isRequired
            isInvalid={!chartFieldId}
            isDisabled={isConfigDisabled}
        >
            <FormControl.Label>Chart Field ID</FormControl.Label>
            <TextInput
                value={chartFieldId}
                name="chartFieldId"
                onChange={onChange}
            />
            <FormControl.HelpText>
            Provide the ID of the field you
            want to use for chart input.
            </FormControl.HelpText>
            {!chartFieldId && (
                <FormControl.ValidationMessage>
    Please, provide a field ID.
                </FormControl.ValidationMessage>
            )}
        </FormControl>
    )
}

const SetContentTypeName = ({
    appInstallationParameters,
    onChange
}: SetParamInputProps) => {
    const {
        contentTypeName,
        isConfigDisabled
    } = appInstallationParameters
    return (
        <FormControl
            isRequired
            isInvalid={!contentTypeName}
            isDisabled={isConfigDisabled}
        >
            <FormControl.Label>Content Type Name</FormControl.Label>
            <TextInput
                value={contentTypeName}
                name="contentTypeName"
                onChange={onChange}
            />
            <FormControl.HelpText>
            Provide the name of the content type
            you want to use for chart input.
            </FormControl.HelpText>
            {!contentTypeName && (
                <FormControl.ValidationMessage>
    Please, provide a content type name.
                </FormControl.ValidationMessage>
            )}
        </FormControl>
    )
}

export const AppInstallationEdit = (props: AppInstallationEditProps) => {
    const { appInstallationParameters, setParameters } = props

    const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = e.target
        setParameters({
            ...appInstallationParameters,
            [name]: value,
        })
    }

    return (
        <Flex flexDirection='column'>
            <Form>

                {/* form control for contentTypeName */}
                <SetContentTypeName
                    appInstallationParameters={appInstallationParameters}
                    onChange={handleInputChange}
                />

                {/* form control for chartFieldId */}
                <SetMermaidVersionFieldId
                    appInstallationParameters={appInstallationParameters}
                    onChange={handleInputChange}
                />

                {/* form control for mermaidVersion */}
                <SetMermaidVersion
                    appInstallationParameters={appInstallationParameters}
                    onChange={handleInputChange}
                />

                {/* form control for chartFieldId */}
                <SetChartFieldId
                    appInstallationParameters={appInstallationParameters}
                    onChange={handleInputChange}
                />


            </Form>
        </Flex>
    )
}