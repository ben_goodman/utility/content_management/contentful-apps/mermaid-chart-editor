import React, {
    type JSX,
    type ChangeEvent,
    type Dispatch,
    type SetStateAction,
} from 'react'
import {
    Checkbox,
    Accordion,
    Paragraph,
    Note,
    List,
    Button,
} from '@contentful/f36-components'
import { Image } from '@contentful/f36-image'
import Code from './CodeBlock'

import fieldSetExampleImage from '../assets/mermaid-field-set-example.png'

import { type AppInstallationParameters, type ConfigAccordionState } from '../locations/ConfigScreen'
import { mermaidChartPropsMin } from '../util/mermaidChartProps'

interface InstallStatusProps {
    appInstallationParameters: AppInstallationParameters,
    onFieldSetClick?: () => void
}
const InstallStatus = ({
    appInstallationParameters,
    onFieldSetClick
}: InstallStatusProps): JSX.Element => {
    const { isFieldAppearanceSet } = appInstallationParameters

    if (isFieldAppearanceSet) {
        return <Note variant="positive">Complete</Note>
    }

    return (
        <>
            <Note variant="warning" title='Field appearance is unset.'>
                <Button variant='secondary' onClick={onFieldSetClick}>Update</Button>
            </Note>
        </>
    )
}

interface ConfigItemProps {
    accordionState: ConfigAccordionState,
    setAccordionState: Dispatch<SetStateAction<ConfigAccordionState>>
    appInstallationParameters: AppInstallationParameters,
    onChange?: (e?: ChangeEvent<HTMLInputElement>) => void,
    onFieldSetClick?: () => void
}

const AutoConfigItem = ({
    appInstallationParameters,
    onChange,
    accordionState,
    setAccordionState,
    onFieldSetClick
}: ConfigItemProps
): JSX.Element => {
    const {
        isContentTypeSet,
        userCheckAutoInstall,
        chartFieldId,
        contentTypeName,
        mermaidVersion,
        mermaidVersionFieldId
    } = appInstallationParameters


    const isExpanded = accordionState.auto

    const handleExpand = () => {
        setAccordionState({auto: true, manual: false})
    }
    const handleCollapse = () => {
        setAccordionState((state) => {
            return {...state, auto: false}
        })
    }

    return (
        <Accordion.Item
            title='Automatic'
            isExpanded={isExpanded}
            onExpand={handleExpand}
            onCollapse={handleCollapse}
        >
            <Paragraph>
                Automatic configuration will:
                <List>
                    <List.Item>
                        Install the&nbsp;
                        <Code.Inline>{contentTypeName}</Code.Inline>&nbsp;
                        content type.
                    </List.Item>
                    <List.Item>
                        Set the appearance of the&nbsp;
                        <Code.Inline>{chartFieldId}</Code.Inline> field
                        to use the custom editor.
                    </List.Item>
                    <List.Item>
                        Set the&nbsp;
                        <Code.Inline>{mermaidVersionFieldId}</Code.Inline> field
                        to &apos;{mermaidVersion}&apos;.
                    </List.Item>
                </List>
            </Paragraph>
            {
                isContentTypeSet
                    ? <InstallStatus
                        appInstallationParameters={appInstallationParameters}
                        onFieldSetClick={onFieldSetClick}
                    />
                    : <Checkbox
                        name="install-custom-type"
                        id="install-custom-type"
                        isChecked={userCheckAutoInstall}
                        onChange={onChange}
                        isDisabled={isContentTypeSet}
                    >
                        <span>
                        Install the&nbsp;
                            <Code.Inline>{contentTypeName}</Code.Inline>&nbsp;
                        content type.
                        </span>
                    </Checkbox>
            }
        </Accordion.Item>
    )
}

const ManualConfigItem = ({
    appInstallationParameters,
    accordionState,
    setAccordionState
}: ConfigItemProps
): JSX.Element => {
    const isExpanded = accordionState.manual
    const {
        chartFieldId,
        mermaidVersionFieldId
    } = appInstallationParameters

    const handleExpand = () => {
        setAccordionState({auto: false, manual: true})
    }
    const handleCollapse = () => {
        setAccordionState((state) => {
            return {...state, manual: false}
        })
    }
    return (
        <Accordion.Item
            title="Manual"
            isExpanded={isExpanded}
            onExpand={handleExpand}
            onCollapse={handleCollapse}
        >
            <Paragraph>
                Install the app to your space.
                Once the app is installed, create a content type
                (or update an exiting one) to contain the following fields:
                <List>
                    <List.Item>
                        <Code.Inline>{chartFieldId}</Code.Inline> (Long text)
                    </List.Item>
                    <List.Item>
                        <Code.Inline>
                            {mermaidVersionFieldId}
                        </Code.Inline> (Symbol)
                    </List.Item>
                </List>
                <Code.Block code={JSON.stringify(mermaidChartPropsMin(appInstallationParameters), null, 2)} language='json' />
            </Paragraph>
            <Paragraph>
                Then set the appearance of the&nbsp;
                <Code.Inline>{chartFieldId}</Code.Inline> field
                to use the custom editor.
                <Image
                    height="281px"
                    width="500px"
                    src={fieldSetExampleImage}
                    alt='Example of setting the `code` field editor.'
                />
            </Paragraph>
        </Accordion.Item>
    )
}

const ConfigItems = { Manual: ManualConfigItem, Auto: AutoConfigItem }

export default ConfigItems